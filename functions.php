<?php
function monsterdev_script_enqueue(){
    wp_enqueue_style('normalize', get_template_directory_uri().'/css/normalize.css', array(), '7.0.0', 'all');
    wp_enqueue_style('monsterdev', get_template_directory_uri().'/css/monsterdev.css', array(), '1.0.0', 'all');
    wp_enqueue_style( 'wpb-google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300', false);
    wp_enqueue_script('monsterdev', get_template_directory_uri().'/js/monsterdev.js', array(), '1.0.0', true);
}

function monsterdev_theme_setup(){
    add_theme_support('menus');
    register_nav_menu('superior', 'Superior header navigation');
    register_nav_menu('primary', 'Primary header navigation');
}

add_action('wp_enqueue_scripts', 'monsterdev_script_enqueue');
add_action('init', 'monsterdev_theme_setup');